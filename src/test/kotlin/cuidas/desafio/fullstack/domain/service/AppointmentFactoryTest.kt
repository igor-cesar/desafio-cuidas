package cuidas.desafio.fullstack.domain.service

import cuidas.desafio.fullstack.domain.model.Appointment
import cuidas.desafio.fullstack.domain.model.Person
import java.util.*
import kotlin.random.Random

fun generateAppointment(
    id: UUID = UUID.randomUUID(),
    datetime: Long = Random.nextLong(),
    person: Person = generatePerson()
) = Appointment(
    id = id,
    datetime = datetime,
    person = person
)

fun generatePerson(
    name: String = randomString(10, 50),
    email: String = randomEmail(),
    phone: String = randomPhone()
) = Person(
    name = name,
    email = email,
    phone = phone
)

fun randomPhone(): String {
    return (0..11)
        .map { Random.nextInt(0, 9) }
        .joinToString(separator = "") { it.toString() }
}

fun randomString(min: Int, max: Int): String {
    val length = Random.nextInt(min, max)
    val bytes = Random.nextBytes(length)
    return bytes.map { it.toChar() }.joinToString(separator = "")
}

fun randomEmail(): String {
    val address = randomString(5, 20)
    val mail = randomString(3, 6)
    return "$address@$mail.com"
}