package cuidas.desafio.fullstack.domain.service

import cuidas.desafio.fullstack.domain.model.Appointment
import cuidas.desafio.fullstack.domain.model.END_TIME
import cuidas.desafio.fullstack.domain.model.START_TIME
import cuidas.desafio.fullstack.domain.repository.AppointmentDatetimeConflictException
import cuidas.desafio.fullstack.domain.repository.AppointmentDatetimeNotFoundException
import cuidas.desafio.fullstack.domain.repository.AppointmentNotFoundException
import cuidas.desafio.fullstack.domain.repository.AppointmentRepository
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.slot
import io.vertx.junit5.VertxExtension
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*
import kotlin.random.Random

@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AppointmentServiceTest {

    private lateinit var appointmentRepository: AppointmentRepository
    private lateinit var appointmentService: AppointmentService

    @BeforeEach
    fun prepare() {
        appointmentRepository = mockk(AppointmentRepository::javaClass.name)
        appointmentService = AppointmentService(appointmentRepository)
    }

    @Test
    fun `Schedule an appointment should save it`() = runBlocking {
        val person = generatePerson()
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
        calendar.set(Calendar.HOUR_OF_DAY, 12)
        val datetime = calendar.timeInMillis

        val appointmentSlot = slot<Appointment>()
        coEvery {
            appointmentRepository.save(capture(appointmentSlot))
        } answers {
            Assertions.assertEquals(appointmentSlot.captured.datetime, datetime)
            Assertions.assertEquals(appointmentSlot.captured.person, person)
        }

        coEvery {
            appointmentRepository.getByDatetime(datetime)
        } throws AppointmentDatetimeNotFoundException(datetime)

        val appointment = appointmentService.schedule(person, datetime)

        Assertions.assertEquals(appointment.datetime, datetime)
        Assertions.assertEquals(appointment.person, person)
    }

    @Test
    fun `Schedule a duplicated appointment should throw exception`() = runBlocking {
        val person = generatePerson()
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
        calendar.set(Calendar.HOUR_OF_DAY, 12)
        val datetime = calendar.timeInMillis

        coEvery {
            appointmentRepository.getByDatetime(datetime)
        } returns generateAppointment()

        Assertions.assertThrows(AppointmentDatetimeConflictException::class.java) {
            runBlocking { appointmentService.schedule(person, datetime) }
        }
        return@runBlocking
    }

    @Test
    fun `Schedule an appointment on weekend should throw exception`() = runBlocking {
        val person = generatePerson()
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY)
        calendar.set(Calendar.HOUR_OF_DAY, 12)
        val datetime = calendar.timeInMillis

        Assertions.assertThrows(AppointmentDatetimeOutException::class.java) {
            runBlocking { appointmentService.schedule(person, datetime) }
        }
        return@runBlocking
    }

    @Test
    fun `Schedule an appointment at start time should save it`() = runBlocking {
        val person = generatePerson()
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
        calendar.set(Calendar.HOUR_OF_DAY, START_TIME / 3600000)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        val datetime = calendar.timeInMillis

        val appointmentSlot = slot<Appointment>()
        coEvery {
            appointmentRepository.save(capture(appointmentSlot))
        } answers {
            Assertions.assertEquals(appointmentSlot.captured.datetime, datetime)
            Assertions.assertEquals(appointmentSlot.captured.person, person)
        }

        coEvery {
            appointmentRepository.getByDatetime(datetime)
        } throws AppointmentDatetimeNotFoundException(datetime)

        val appointment = appointmentService.schedule(person, datetime)

        Assertions.assertEquals(appointment.datetime, datetime)
        Assertions.assertEquals(appointment.person, person)
    }

    @Test
    fun `Schedule an appointment before 10h should throw exception`() = runBlocking {
        val person = generatePerson()
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
        calendar.set(Calendar.HOUR_OF_DAY, START_TIME / 3600000)
        calendar.set(Calendar.MINUTE, -1)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        val datetime = calendar.timeInMillis

        Assertions.assertThrows(AppointmentDatetimeOutException::class.java) {
            runBlocking { appointmentService.schedule(person, datetime) }
        }
        return@runBlocking
    }

    @Test
    fun `Schedule an appointment at end time should save it`() = runBlocking {
        val person = generatePerson()
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
        calendar.set(Calendar.HOUR_OF_DAY, END_TIME / 3600000)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        val datetime = calendar.timeInMillis

        val appointmentSlot = slot<Appointment>()
        coEvery {
            appointmentRepository.save(capture(appointmentSlot))
        } answers {
            Assertions.assertEquals(appointmentSlot.captured.datetime, datetime)
            Assertions.assertEquals(appointmentSlot.captured.person, person)
        }

        coEvery {
            appointmentRepository.getByDatetime(datetime)
        } throws AppointmentDatetimeNotFoundException(datetime)

        val appointment = appointmentService.schedule(person, datetime)

        Assertions.assertEquals(appointment.datetime, datetime)
        Assertions.assertEquals(appointment.person, person)
    }

    @Test
    fun `Schedule an appointment after end time should throw exception`() = runBlocking {
        val person = generatePerson()
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
        calendar.set(Calendar.HOUR_OF_DAY, END_TIME / 3600000)
        calendar.set(Calendar.MINUTE, 1)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        val datetime = calendar.timeInMillis

        Assertions.assertThrows(AppointmentDatetimeOutException::class.java) {
            runBlocking { appointmentService.schedule(person, datetime) }
        }
        return@runBlocking
    }

    @Test
    fun `Cancel an appointment should remove it`() = runBlocking {
        val appointment = generateAppointment()

        coEvery {
            appointmentRepository.remove(appointment.id)
        } returns appointment

        val canceledAppointment = appointmentService.cancel(appointment.id)

        Assertions.assertEquals(appointment, canceledAppointment)
    }

    @Test
    fun `Cancel a non existent appointment should throw exception`() = runBlocking {
        val id = UUID.randomUUID()

        coEvery {
            appointmentRepository.remove(id)
        } throws AppointmentNotFoundException(id)

        val exception = Assertions.assertThrows(AppointmentNotFoundException::class.java) {
            runBlocking { appointmentService.cancel(id) }
        }

        Assertions.assertEquals(exception.id, id)
    }

    @Test
    fun `Get appointments with empty repository should return an empty list`() = runBlocking {
        coEvery {
            appointmentRepository.getAll(0)
        } returns listOf()

        val gottenAppointments = appointmentService.getAll(0)

        Assertions.assertTrue(gottenAppointments.isEmpty())
    }

    @Test
    fun `Get all appointments`() = runBlocking {
        val appointments = (0..Random.nextInt(1, 20)).map {
            generateAppointment()
        }

        coEvery {
            appointmentRepository.getAll(0)
        } returns appointments

        val gottenAppointments = appointmentService.getAll(0)

        Assertions.assertTrue(appointments.size == gottenAppointments.size)
        Assertions.assertTrue(appointments.containsAll(gottenAppointments))
    }
}