package cuidas.desafio.fullstack

import cuidas.desafio.fullstack.api.AppointmentHttpApi
import cuidas.desafio.fullstack.domain.repository.AppointmentRepository
import cuidas.desafio.fullstack.domain.service.AppointmentService
import cuidas.desafio.fullstack.infrastructure.repository.AppointmentRepositoryMongodbImpl
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpServer
import io.vertx.core.logging.LoggerFactory
import io.vertx.ext.mongo.MongoClient
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.CorsHandler
import io.vertx.kotlin.core.http.listenAwait
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import java.util.HashSet



class MainVerticle: CoroutineVerticle() {
    private val logger = LoggerFactory.getLogger(javaClass)

    private val server: HttpServer by lazy {
        vertx.createHttpServer()
    }

    private val appointmentRepository: AppointmentRepository by lazy {
        val dbClient = MongoClient.createShared(vertx, jsonObjectOf(
            "connection_string" to "mongodb://desafiocuidas:1yf4by3cNr3wB8F@ds011902.mlab.com:11902/heroku_mw72b0mn"
        ))
        AppointmentRepositoryMongodbImpl(dbClient)
    }

    private val appointmentService: AppointmentService by lazy {
        AppointmentService(
            appointmentRepository = appointmentRepository
        )
    }

    private val appointmentHttpApi: AppointmentHttpApi by lazy {
        AppointmentHttpApi(
            vertx = vertx,
            appointmentService = appointmentService
        )
    }

    override suspend fun start() {
        logger.info("Starting Http Server Verticle")
        val port = System.getenv("PORT")?.toInt() ?: 8080

        val router = Router.router(vertx)

        val allowedHeaders = setOf(
            "x-requested-with",
            "Access-Control-Allow-Origin",
            "origin",
            "Content-Type",
            "accept",
            "X-PINGARUNER"
        )

        val allowedMethods = setOf(
            HttpMethod.GET,
            HttpMethod.POST,
            HttpMethod.OPTIONS,
            HttpMethod.DELETE,
            HttpMethod.PATCH,
            HttpMethod.PUT
        )

        router.route().handler(CorsHandler.create("*").allowedHeaders(allowedHeaders).allowedMethods(allowedMethods))

        val apiRouter = Router.router(vertx)
        apiRouter.mountSubRouter("/appointment", appointmentHttpApi.router)

        router.mountSubRouter("/api", apiRouter)

        server.requestHandler(router).listenAwait(port)

        logger.info("Http Server: Listening on ${server.actualPort()} port")
    }
}