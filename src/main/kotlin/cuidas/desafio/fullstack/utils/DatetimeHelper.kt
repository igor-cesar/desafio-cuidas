package cuidas.desafio.fullstack.utils

import cuidas.desafio.fullstack.domain.model.END_TIME
import cuidas.desafio.fullstack.domain.model.START_TIME
import java.util.*

fun isInWorkingTime(datetime: Long): Boolean {
    val day = extractDay(datetime)
    return datetime >= day + START_TIME && datetime <= day + END_TIME
}

fun isInWeekday(datetime: Long): Boolean {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = datetime
    return calendar.get(Calendar.DAY_OF_WEEK) in listOf(
        Calendar.MONDAY,
        Calendar.TUESDAY,
        Calendar.WEDNESDAY,
        Calendar.THURSDAY,
        Calendar.FRIDAY
    )
}

fun extractDay(datetime: Long): Long {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = datetime

    calendar.set(Calendar.HOUR_OF_DAY, 0)
    calendar.set(Calendar.MINUTE, 0)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)
    return calendar.timeInMillis
}