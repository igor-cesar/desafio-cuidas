package cuidas.desafio.fullstack.utils

import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.http.HttpServerResponse
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Route
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

fun Route.suspendingHandler(block: suspend (rc: RoutingContext) -> Unit): Route {
    this.handler { rc ->
        val vertx = rc.vertx()
        GlobalScope.launch(vertx.dispatcher()) {
            try {
                block(rc)
            } catch (e: Exception) {
                rc.fail(e)
            }
        }
    }.failureHandler {
        defaultSuspendingFailureHandler(it)
    }
    return this
}

private const val DEFAULT_ERROR_MSG = "An unexpected error has occurred"

private fun defaultSuspendingFailureHandler(rc: RoutingContext) {
    val exception = rc.failure() as Exception?
    val message = (exception?.message ?: exception?.cause?.message) ?: DEFAULT_ERROR_MSG

    if (!rc.response().ended()) {
        rc.response().setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).error(message)
    }
    throw rc.failure()
}

/**
 * Methods to end an http request
 */

fun HttpServerResponse.end(jsonObject: JsonObject) {
    this.putHeader("Content-Type", "application/json")
        .end(jsonObject.toString())
}

fun HttpServerResponse.end(jsonArray: JsonArray) {
    this.putHeader("Content-Type", "application/json")
        .end(jsonArray.toString())
}

fun HttpServerResponse.error(error: String?) {
    this.putHeader("Content-Type","application/json")
        .end(jsonObjectOf("error"  to error).toString())
}

fun HttpServerResponse.error(error: JsonObject) {
    this.putHeader("Content-Type","application/json")
        .end(jsonObjectOf("error"  to error.toString()).toString())
}

fun HttpServerResponse.error(error: JsonArray) {
    this.putHeader("Content-Type", "application/json")
        .end(jsonObjectOf("error"  to error.toString()).toString())
}
