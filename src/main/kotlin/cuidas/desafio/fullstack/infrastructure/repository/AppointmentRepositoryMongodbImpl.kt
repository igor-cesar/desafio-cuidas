package cuidas.desafio.fullstack.infrastructure.repository

import cuidas.desafio.fullstack.domain.model.Appointment
import cuidas.desafio.fullstack.domain.repository.AppointmentDatetimeNotFoundException
import cuidas.desafio.fullstack.domain.repository.AppointmentNotFoundException
import cuidas.desafio.fullstack.domain.repository.AppointmentRepository
import io.vertx.core.logging.LoggerFactory
import io.vertx.ext.mongo.MongoClient
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.ext.mongo.findAwait
import io.vertx.kotlin.ext.mongo.findOneAndDeleteAwait
import io.vertx.kotlin.ext.mongo.findOneAwait
import io.vertx.kotlin.ext.mongo.saveAwait
import java.util.*

const val APPOINTMENT_COLLECTION = "cuidas.desafio.appointment"

class AppointmentRepositoryMongodbImpl(
    private val dbClient: MongoClient
): AppointmentRepository {
    private val logger = LoggerFactory.getLogger(javaClass)

    override suspend fun save(appointment: Appointment) {
        val appointmentJson = appointment.toJson()
        dbClient.saveAwait(APPOINTMENT_COLLECTION, appointmentJson)

        logger.info("Appointment Repository: save appointment. \n" +
                appointmentJson.encodePrettily())
    }

    override suspend fun remove(id: UUID): Appointment {
        val query = jsonObjectOf("id" to id.toString())
        val appointmentJson = dbClient.findOneAndDeleteAwait(APPOINTMENT_COLLECTION, query)
            ?: throw AppointmentNotFoundException(id)

        logger.info("Appointment Repository: remove appointment. \n" +
                appointmentJson.encodePrettily())

        return Appointment(appointmentJson)
    }

    override suspend fun getAll(since: Long): List<Appointment> {
        val query = jsonObjectOf("datetime" to jsonObjectOf(
            "\$gt" to since
        ))
        val appointmentsJson = dbClient.findAwait(APPOINTMENT_COLLECTION, query)

        logger.info("Appointment Repository: get all appointments")

        return appointmentsJson.map { Appointment(it) }
    }

    override suspend fun getByDatetime(datetime: Long): Appointment {
        val query = jsonObjectOf("datetime" to datetime)
        val appointmentsJson = dbClient.findAwait(APPOINTMENT_COLLECTION, query)

        if (appointmentsJson.isEmpty()) {
            throw AppointmentDatetimeNotFoundException(datetime)
        }

        logger.info("Appointment Repository: get all appointments")

        return Appointment(appointmentsJson.first())
    }

    override suspend fun getByDatetimeRange(min: Long, max: Long): List<Appointment> {
        val query = jsonObjectOf("datetime" to jsonObjectOf(
            "\$lt" to max,
            "\$gt" to min
        ))
        val appointmentsJson = dbClient.findAwait(APPOINTMENT_COLLECTION, query)

        logger.info("Appointment Repository: get appointments before $max and later than $min")

        return appointmentsJson.map { Appointment(it) }
    }
}