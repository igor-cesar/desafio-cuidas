package cuidas.desafio.fullstack.domain.repository

import cuidas.desafio.fullstack.domain.model.Appointment
import java.util.*

interface AppointmentRepository {
    suspend fun save(appointment: Appointment)
    suspend fun remove(id: UUID): Appointment
    suspend fun getAll(since: Long): List<Appointment>
    suspend fun getByDatetime(datetime: Long): Appointment
    suspend fun getByDatetimeRange(min: Long, max: Long): List<Appointment>
}