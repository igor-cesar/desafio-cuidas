package cuidas.desafio.fullstack.domain.repository

import java.lang.Exception
import java.util.*

class AppointmentNotFoundException(
    val id: UUID
): Exception("Appointment with id=$id not found")

class AppointmentDatetimeNotFoundException(
    val datetime: Long
): Exception("Appointment with datetime=$datetime not found")

class AppointmentConflictException(
    val id: UUID
): Exception("Appointment with id=$id already exists")

class AppointmentDatetimeConflictException(
    val datetime: Long
): Exception("Appointment with datetime=$datetime already exists")