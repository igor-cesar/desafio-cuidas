package cuidas.desafio.fullstack.domain.service

import cuidas.desafio.fullstack.domain.model.Appointment
import cuidas.desafio.fullstack.domain.model.Person
import cuidas.desafio.fullstack.domain.repository.AppointmentDatetimeConflictException
import cuidas.desafio.fullstack.domain.repository.AppointmentDatetimeNotFoundException
import cuidas.desafio.fullstack.domain.repository.AppointmentRepository
import cuidas.desafio.fullstack.utils.isInWeekday
import cuidas.desafio.fullstack.utils.isInWorkingTime
import java.util.*

class AppointmentService(
    private val appointmentRepository: AppointmentRepository
) {
    suspend fun schedule(person: Person, datetime: Long): Appointment {
        if (!isDatetimeAllowed(datetime)) {
            throw AppointmentDatetimeOutException()
        }

        val storedAppointment = try {
            appointmentRepository.getByDatetime(datetime)
        } catch (e: AppointmentDatetimeNotFoundException) {
            null
        }

        if (storedAppointment != null) {
            throw AppointmentDatetimeConflictException(datetime)
        }

        val appointment = Appointment(
            id = UUID.randomUUID(),
            person = person,
            datetime = datetime
        )
        appointmentRepository.save(appointment)

        return appointment
    }

    suspend fun cancel(id: UUID): Appointment {
        return appointmentRepository.remove(id)
    }

    suspend fun getAll(since: Long): List<Appointment> {
        return appointmentRepository.getAll(since)
    }

    suspend fun getByDay(datetime: Long): List<Appointment> {
        val min = datetime
        val max = datetime + 86400000
        return appointmentRepository.getByDatetimeRange(min, max)
    }

    private fun isDatetimeAllowed(datetime: Long): Boolean {
        return isInWorkingTime(datetime) && isInWeekday(datetime)
    }
}