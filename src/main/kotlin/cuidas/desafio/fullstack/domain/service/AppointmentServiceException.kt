package cuidas.desafio.fullstack.domain.service


class AppointmentDatetimeOutException
    : Exception("Scheduling is out of working time")