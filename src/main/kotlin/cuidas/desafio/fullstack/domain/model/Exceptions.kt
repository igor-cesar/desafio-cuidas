package cuidas.desafio.fullstack.domain.model

class JsonParseException(message: String): Exception(message)