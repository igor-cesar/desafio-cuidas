package cuidas.desafio.fullstack.domain.model

import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.jsonObjectOf
import java.util.*

data class Appointment(
    val id: UUID,
    val person: Person,
    val datetime: Long
) {
    constructor(appointmentJson: JsonObject): this(
        id = appointmentJson.getString("id")?.let { UUID.fromString(it) }
            ?: throw JsonParseException("Field 'id' not found in 'appointment'"),
        person = appointmentJson.getJsonObject("person")?.let { Person(it) }
            ?: throw JsonParseException("Field 'person' not found in 'appointment'"),
        datetime = appointmentJson.getLong("datetime")
            ?: throw JsonParseException("Field 'datetime' not found in 'appointment'")
    )

    fun toJson(): JsonObject = jsonObjectOf(
        "id" to id.toString(),
        "person" to person.toJson(),
        "datetime" to datetime
    )
}

const val START_TIME = 36000000 // 10:00
const val END_TIME = 64800000 // 18:00
const val APPOINTMENT_DURATION = 1200000 // 1h
const val MIN_DATE = 0 // Minimum slot of time to schedule an appointment: current time
const val MAX_DATE = 2592000000 // Maximum allowed slot of time to scheule an appointment: 30 days