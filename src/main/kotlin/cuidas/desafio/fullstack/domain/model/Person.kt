package cuidas.desafio.fullstack.domain.model

import io.vertx.core.json.JsonObject

class Person(
    val name: String,
    val email: String,
    val phone: String
) {
    constructor(personJson: JsonObject): this(
        name = personJson.getString("name")
            ?: throw JsonParseException("Field 'name' not found in 'person'"),
        email = personJson.getString("email")
            ?: throw JsonParseException("Field 'email' not found in 'person'"),
        phone = personJson.getString("phone")
            ?: throw JsonParseException("Field 'phone' not found in 'person'")
    )

    fun toJson(): JsonObject = JsonObject.mapFrom(this)
}
