package cuidas.desafio.fullstack.api

import cuidas.desafio.fullstack.domain.model.JsonParseException
import cuidas.desafio.fullstack.domain.model.Person
import cuidas.desafio.fullstack.domain.repository.AppointmentConflictException
import cuidas.desafio.fullstack.domain.repository.AppointmentDatetimeConflictException
import cuidas.desafio.fullstack.domain.repository.AppointmentNotFoundException
import cuidas.desafio.fullstack.domain.service.AppointmentDatetimeOutException
import cuidas.desafio.fullstack.domain.service.AppointmentService
import cuidas.desafio.fullstack.utils.end
import cuidas.desafio.fullstack.utils.error
import cuidas.desafio.fullstack.utils.suspendingHandler
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.logging.LoggerFactory
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import java.util.*

class AppointmentHttpApi(
    vertx: Vertx,
    private val appointmentService: AppointmentService
) {
    private val logger = LoggerFactory.getLogger(javaClass)
    val router: Router = Router.router(vertx).apply {
        this.post().handler(BodyHandler.create())
    }.apply {
        this.get("/:since").suspendingHandler { getAll(it) }
        this.get("/day/:datetime").suspendingHandler { getByDay(it) }
        this.post("/").suspendingHandler { schedule(it) }
        this.delete("/:id").suspendingHandler { cancel(it) }
    }

    suspend fun getAll(rc: RoutingContext) {
        logger.info("GET /api/appointment/")
        try {
            val since = rc.request().getParam("since")
            val appointments = appointmentService.getAll(since.toLong())

            rc.response().end(JsonArray(appointments.map { it.toJson() }))

        } catch (e: JsonParseException) {
            rc.response().setStatusCode(HttpResponseStatus.BAD_REQUEST.code()).error(e.message)
        } catch (e: AppointmentConflictException) {
            rc.response().setStatusCode(HttpResponseStatus.CONFLICT.code()).error(e.message)
        } catch (e: AppointmentDatetimeOutException) {
            rc.response().setStatusCode(HttpResponseStatus.BAD_REQUEST.code()).error(e.message)
        } catch (e: AppointmentDatetimeConflictException) {
            rc.response().setStatusCode(HttpResponseStatus.CONFLICT.code()).error(e.message)
        }
    }

    suspend fun getByDay(rc: RoutingContext) {
        logger.info("GET /api/appointment/day/:datetime")
        try {
            val datetime = rc.request().getParam("datetime")
            val appointments = appointmentService.getByDay(datetime.toLong())

            rc.response().end(JsonArray(appointments.map { it.toJson() }))

        } catch (e: JsonParseException) {
            rc.response().setStatusCode(HttpResponseStatus.BAD_REQUEST.code()).error(e.message)
        } catch (e: AppointmentConflictException) {
            rc.response().setStatusCode(HttpResponseStatus.CONFLICT.code()).error(e.message)
        }
    }

    suspend fun schedule(rc: RoutingContext) {
        logger.info("POST /api/appointment/")
        try {
            val body = rc.bodyAsJson
            val person = body.getJsonObject("person")?.let { Person(it) }
                ?: throw JsonParseException("Field 'person' not found in 'appointment'")
            val datetime = body.getLong("datetime")
                ?: throw JsonParseException("Field 'datetime' not found in 'appointment'")

            val appointment = appointmentService.schedule(person, datetime)

            rc.response().end(appointment.toJson())

        } catch (e: JsonParseException) {
            rc.response().setStatusCode(HttpResponseStatus.BAD_REQUEST.code()).error(e.message)
        } catch (e: AppointmentConflictException) {
            rc.response().setStatusCode(HttpResponseStatus.CONFLICT.code()).error(e.message)
        } catch (e: AppointmentDatetimeOutException) {
            rc.response().setStatusCode(HttpResponseStatus.BAD_REQUEST.code()).error(e.message)
        } catch (e: AppointmentDatetimeConflictException) {
            rc.response().setStatusCode(HttpResponseStatus.CONFLICT.code()).error(e.message)
        }
    }

    suspend fun cancel(rc: RoutingContext) {
        logger.info("DELETE /api/appointment/:id")
        try {
            val id = rc.request().getParam("id")?.let { UUID.fromString(it) }
                ?: throw IllegalArgumentException("Param 'id' is required")

            val appointment = appointmentService.cancel(id)

            rc.response().end(appointment.toJson())

        } catch (e: IllegalArgumentException) {
            rc.response().setStatusCode(HttpResponseStatus.BAD_REQUEST.code()).error(e.message)
        } catch (e: AppointmentNotFoundException) {
            rc.response().setStatusCode(HttpResponseStatus.NOT_FOUND.code()).error(e.message)
        }
    }
}