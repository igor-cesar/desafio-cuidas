export class DatetimeHelper {

    static formattedTime(value: number): string {
        const datetime = new Date(value)
        return formatTime(datetime)
    }

    static formattedDate(value: number): string {
        const datetime = new Date(value)
        return formatDate(datetime)
    }

    static formattedDatetime(value: number): string {
        const datetime = new Date(value)
        return formatDateTime(datetime)
    }

    static previousWeekday(value: number): number {
        const datetime = new Date(value)

        if (datetime.getDay() == 0) {
            return value - 2 * 86400000
        }
        if (datetime.getDay() == 1) {
            return value - 3 * 86400000
        }
        return value - 86400000
    }

    static nextWeekday(value: number): number {
        const datetime = new Date(value)

        if (datetime.getDay() == 6) {
            return value + 2 * 86400000
        }
        if (datetime.getDay() == 5) {
            return value + 3 * 86400000
        }
        return value + 86400000
    }

    static today(): number {
        const datetime = new Date()
        datetime.setHours(0, 0, 0, 0)
        return datetime.getTime()
    }

    static soonerWeekday(): number {
        const datetime = new Date()
        datetime.setHours(0, 0, 0, 0)
        const value = datetime.getTime()

        if (datetime.getDay() == 6) {
            return value + 2 * 86400000
        }
        if (datetime.getDay() == 7) {
            return value + 86400000
        }

        return value
    }

    static now(): number {
        const datetime = new Date()
        return datetime.getTime()
    }

    static extractDay(value: number): number {
        const datetime = new Date(value)
        datetime.setHours(0, 0, 0, 0)
        return datetime.getTime()
    }

    static timezone(): number {
        return new Date().getTimezoneOffset() * 60000
    }
}

function formatTime(time: Date): string {
    let hours = time.getHours().toFixed()
    hours = hours.length == 1 ? `0${hours}` : hours
    let minutes = time.getMinutes().toFixed()
    minutes = minutes.length == 1 ? `0${minutes}` : minutes
    return `${hours}:${minutes}`
}

function formatDate(date: Date): string {
    return `${weekday[date.getDay()]}, ${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}`
}

function formatDateTime(datetime: Date): string {
    return `${weekday[datetime.getDay()]}, ${datetime.getDate()} ${months[datetime.getMonth()]} ` +
        `${datetime.getFullYear()}, ${formatTime(datetime)}`
}

const weekday = [
    "Dom",
    "Seg",
    "Ter",
    "Qua",
    "Qui",
    "Sex",
    "Sab"
]

const months = [
    "Jan",
    "Fev",
    "Mar",
    "Abr",
    "Mai",
    "Jun",
    "Jul",
    "Ago",
    "Set",
    "Out",
    "Nov",
    "Dez"
]