const API_URL = "https://desafio-cuidas.herokuapp.com/api"

export class HttpApiClient {

    static async get(endpoint: string) {
        let response
        try {
            response = await fetch(API_URL + endpoint)
        } catch (e) {
            throw { status: 0,
                    msg: 'Ocorreu um erro ao tentar realizar requisição'}
        }

        const json = await response.json()

        if (response.ok) {
            return json
        } else {
            throw { status: response.status,
                    msg: json.error ? json.error : 'Ocorreu um erro durante a requisição'}
        }
    }

    static async post(  endpoint: string,
                        data: any = {},
                        headers: any = { 'Content-type' : 'application/json' }) {
        let response
        try {
            response = await fetch(API_URL + endpoint, {
                headers,
                method: 'post',
                body: data,
            })
        } catch (e) {
            throw { status: 0, msg: 'Ocorreu um erro ao tentar realizar requisição'}
        }

        const json = await response.json()

        if (response.ok) {
            return json
        } else {
            throw { status: response.status,
                    msg: json.error ? json.error : 'Ocorreu um erro durante a requisição'}
        }
    }

    static async delete(endpoint: string,
                        headers: any = { 'Content-type' : 'application/json' }) {
        let response
        try {
            response = await fetch(API_URL + endpoint, {
                headers,
                method: 'delete',
            })
        } catch (e) {
            throw { status: 0, msg: 'Ocorreu um erro ao tentar realizar requisição'}
        }

        const json = await response.json()

        if (response.ok) {
            return json
        } else {
            throw { status: response.status,
                    msg: json.error ? json.error : 'Ocorreu um erro durante a requisição'}
        }
    }
}
