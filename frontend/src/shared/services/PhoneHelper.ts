export class PhoneHelper {

    static formattedPhone(value: string) {
        const ddd = value.substring(0, 2)
        const phone1 = value.substring(2, 7)
        const phone2 = value.substring(7)
        return `(${ddd}) ${phone1}-${phone2}`
    }
}