import Vue from "vue"
import VueRouter from "vue-router"
import PatientScreen from "@/appointment/screens/PatientScreen.vue"

Vue.use(VueRouter)

const routes = [
    {
        path: "/",
        redirect: "/patient"
    },
    {
        path: "/patient",
        name: "patient",
        component: PatientScreen
    },
    {
        path: "/admin",
        name: "admin",
        component: () => import("../appointment/screens/AdminScreen.vue")
    }
]

const router = new VueRouter({
    mode: "history",
    routes
})

export default router
