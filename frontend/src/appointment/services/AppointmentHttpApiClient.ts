import { Appointment } from "@/appointment/models/Appointment"
import { HttpApiClient } from "@/shared/services/HttpApiClient"
import { Patient } from '@/appointment/models/Patient'
import { DatetimeHelper } from '@/shared/services/DatetimeHelper'

const APPOINTMENT_API_ENDPOINT = "/appointment"

export class AppointmentHttpApiClient {

    static async getAll(since: number): Promise<Appointment[]> {
        const appointments: Appointment[] = await HttpApiClient.get(`${APPOINTMENT_API_ENDPOINT}/${since}`)
        return appointments.map((appointment: Appointment) => AppointmentHttpApiJsonConverter.fromJson(appointment))
    }

    static async getByDay(datetime: number): Promise<Appointment[]> {
        const appointments: Appointment[] = await HttpApiClient.get(`${APPOINTMENT_API_ENDPOINT}/day/${datetime}`)
        return appointments.map((appointment: Appointment) => AppointmentHttpApiJsonConverter.fromJson(appointment))
    }

    static async schedule(appointment: Appointment): Promise<Appointment> {
        const data = AppointmentHttpApiJsonConverter.toJson(appointment)
        const scheduledAppointment: Appointment = await HttpApiClient.post(APPOINTMENT_API_ENDPOINT, data)
        return AppointmentHttpApiJsonConverter.fromJson(scheduledAppointment)
    }

    static async cancel(appointment: Appointment): Promise<Appointment> {
        const canceledAppointment: Appointment = await HttpApiClient.delete(`${APPOINTMENT_API_ENDPOINT}/${appointment.id}`)
        return AppointmentHttpApiJsonConverter.fromJson(canceledAppointment)
    }
}

class AppointmentHttpApiJsonConverter {
    static fromJson(appointment: Appointment) {
        return new Appointment(
            appointment.id,
            PatientHttpApiJsonConverter.patientFromJson(appointment.person),
            appointment.datetime + DatetimeHelper.timezone()
        )
    }

    static toJson(appointment: Appointment) {
        return JSON.stringify(appointment.toJson())
    }
}

class PatientHttpApiJsonConverter {
    static patientFromJson(patient: Patient) {
        if (patient) {
            return new Patient(
                patient.id,
                patient.name,
                patient.email,
                patient.phone
            )
        } else {
            throw Error("Patient cannot be undefined")
        }
    }
}
