import { Appointment } from '@/appointment/models/Appointment'
import { DatetimeHelper } from '@/shared/services/DatetimeHelper'

export class AppointmentFormatter {
    static orderByDatetime(appointments: Appointment[]): Appointment[] {
        return appointments.sort((a1: Appointment, a2: Appointment) => a1.datetime - a2.datetime)
    }

    static groupByDay(appointments: Appointment[]): Map<number, Appointment[]> {
        const appointmentsByDay = new Map<number, Appointment[]>()

        appointments.forEach((appointment: Appointment) => {
            const day = DatetimeHelper.extractDay(appointment.datetime)
            const appointmentsOfThisDay = appointmentsByDay.get(day)
            if (appointmentsOfThisDay) {
                appointmentsOfThisDay.push(appointment)
                appointmentsByDay.set(day, appointmentsOfThisDay)
            } else {
                appointmentsByDay.set(day, [appointment])
            }
        })

        return appointmentsByDay
    }
}
