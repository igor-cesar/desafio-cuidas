import { Patient } from './Patient'
import { DatetimeHelper } from '@/shared/services/DatetimeHelper'
import { END_TIME, START_TIME, APPOINTMENT_DURATION } from './constants'

export class Appointment {
    constructor(
        public id: string,
        public person: Patient,
        public datetime: number
    ) {}

    toJson() {
        return {
            id: this.id,
            person: this.person.toJson(),
            datetime: this.datetime - DatetimeHelper.timezone()
        }
    }

    get formattedDate(): string {
        return DatetimeHelper.formattedDate(this.datetime)
    }
    get formattedTime(): string {
        return DatetimeHelper.formattedTime(this.datetime)
    }
    get formattedDatetime(): string {
        return DatetimeHelper.formattedDatetime(this.datetime)
    }
}



export function getAppointmentSlots(): number[] {
    const dayTime = END_TIME - START_TIME
    const numberOfSlots = Math.floor(dayTime/APPOINTMENT_DURATION)
    const slots = []
    for (let i = 0; i < numberOfSlots; i++) {
        slots.push(START_TIME + i * APPOINTMENT_DURATION)
    }
    return slots
}
