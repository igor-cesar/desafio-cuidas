import { DatetimeHelper } from "@/shared/services/DatetimeHelper"

export class Datetime {
    public formattedDate: string
    public formattedTime: string
    public formattedDatetime: string

    constructor(public value: number) {
        this.formattedDate = DatetimeHelper.formattedDate(this.value)
        this.formattedTime = DatetimeHelper.formattedTime(this.value)
        this.formattedDatetime = DatetimeHelper.formattedDatetime(this.value)
    }
}
