export const START_TIME = 36000000 // 10:00
export const END_TIME = 64800000 // 18:00
export const APPOINTMENT_DURATION = 1200000 // 1h
export const MIN_DATE = 0 // Minimum slot of time to schedule an appointment: current time
export const MAX_DATE = 2592000000 // Maximum allowed slot of time to scheule an appointment: 30 days 