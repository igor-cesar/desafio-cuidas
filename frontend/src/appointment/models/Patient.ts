import { PhoneHelper } from '@/shared/services/PhoneHelper';

export class Patient {
    constructor(
        public id: string = "",
        public name: string = "",
        public email: string = "",
        public phone: string = ""
    ) { }

    toJson() {
        return {
            id: this.id,
            name: this.name,
            email: this.email,
            phone: this.phone
        };
    }

    get formattedPhone(): string {
        return PhoneHelper.formattedPhone(this.phone)
    }
}
